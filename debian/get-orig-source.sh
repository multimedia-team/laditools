#!/bin/sh

BASE_REL=$(dpkg-parsechangelog 2>/dev/null | sed -ne 's/Version: \([0-9.]\+~rc2\)+.*/\1/p')
OLDDIR=${PWD}
GOS_DIR=${OLDDIR}/get-orig-source
GIT_COMMIT='git log --color=never -1 --oneline | cut -d" " -f1'
GIT_DATE='git log --color=never -1 --date=iso | sed -ne "s/Date:\s\+\(.*\).*/\1/p" | cut -d" " -f1 | tr -d "-"'

if [ -z ${BASE_REL} ]; then
	echo 'Please run this script from the sources root directory.'
	exit 1
fi


rm -rf ${GOS_DIR}
mkdir ${GOS_DIR} && cd ${GOS_DIR}
git clone git://repo.or.cz/laditools.git laditools
cd laditools/
LADITOOLS_GIT_COMMIT=$(eval "${GIT_COMMIT}")
LADITOOLS_GIT_DATE=$(eval "${GIT_DATE}")
cd .. && tar cjf \
	${OLDDIR}/swh-lv2_${BASE_REL}+${LADITOOLS_GIT_DATE}.git${LADITOOLS_GIT_COMMIT}.orig.tar.bz2 \
	laditools --exclude=.git
rm -rf ${GOS_DIR}
